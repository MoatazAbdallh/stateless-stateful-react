import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Persons from './components/Persons/Persons';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Persons />, document.getElementById('root'));
registerServiceWorker();
