/**
 * Created by Moataz on 9/29/2018.
 */

import React,{Component} from 'react';
import Person from '../Person/Person';
import PersonsClasses from './Persons.css';
import ErrorBoundary from '../ErrorBoundaries/ErrorBoundary';

class Persons extends Component {

    state = {
        persons: [
            {
                id: 1,
                name: "Koko",
                age: "25"
            }, {
                id: 2,
                name: "Hamada",
                age: 20
            }
        ]
    };

    personClickHandler = (name) => {
        //alert(`My Name ${name}`)
    };

    //Handling Changing in Immutable Way
    nameChangeHandler = (id, event) => {
        const selectedPersonIdx = this.state.persons.findIndex(person=> person.id === id);
        const person = {...this.state.persons[selectedPersonIdx]};
        person.name = event.target.value;
        const persons = [...this.state.persons];
        persons[selectedPersonIdx] = person;
        this.setState((prevState, props) => { //used by this format as setState is executed async
            // so if you will depend on prevState it's recommended to use the above format
            return {persons}
        })
    };

    ageChangeHandler = (id, event) => {
        const selectedPersonIdx = this.state.persons.findIndex(person=> person.id === id);
        const person = {...this.state.persons[selectedPersonIdx]};
        person.age = event.target.value;
        const persons = [...this.state.persons];
        persons[selectedPersonIdx] = person;
        this.setState((prevState, props) => { //used by this format as setState is executed async
            // so if you will depend on prevState it's recommended to use the above format
            return {persons}
        })
    };

    deleteHandler = (id) => {
        const selectedPersonIdx = this.state.persons.findIndex(person=> person.id === id);
        const persons = [...this.state.persons];
        persons.splice(selectedPersonIdx, 1);
        this.setState({persons})
    };

    addHandler = () => {
        this.setState((prevState, props) => { //used by this format as setState is executed async
            // so if you will depend on prevState it's recommended to use the above format
            return {
                persons: [...prevState.persons,
                    {
                        name: '',
                        age: '',
                        id: prevState.persons.length - 1 > -1 ? prevState.persons[prevState.persons.length - 1].id + 1 : 1
                    }]
            }
        })
    };

    render() {
        const persons = this.state.persons.map(person => {
            return <Person key={person.id} name={person.name} age={person.age}
                           click={this.personClickHandler}
                           nameChangeHandler={this.nameChangeHandler.bind(this,person.id)}
                           ageChangeHandler={this.ageChangeHandler.bind(this,person.id)}
                           deleteHandler={this.deleteHandler.bind(this,person.id)}>
                lorem ipsum lorem ipsum lorem
                ipsum</Person>
        });
        return (
            <ErrorBoundary>
                <div className={PersonsClasses.personsWrapper}>
                    {persons}
                    <button onClick={this.addHandler}>Add</button>
                </div>
            </ErrorBoundary>
        )
    }
}

export default Persons;