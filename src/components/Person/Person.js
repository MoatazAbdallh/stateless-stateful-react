/**
 * Created by Moataz on 9/29/2018.
 */
import React from 'react';
import PersonClasses from './Person.css';

const Person = (props) => {
    return (
        <div className={PersonClasses.personWrapper} onClick={props.click.bind(this,props.name)}>
            <input value={props.name} onChange={props.nameChangeHandler}/>
            <input value={props.age} onChange={props.ageChangeHandler}/>
            <div>{props.children}</div>
            <button onClick={props.deleteHandler}>Delete</button>
        </div>
    )
};

export default Person;


