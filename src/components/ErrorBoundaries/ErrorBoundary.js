/**
 * Created by Moataz on 10/7/2018.
 */

import React,{Component} from 'react';

class ErrorBoundary extends Component {
    state = {
        hasError: false,
        errorMessage: ''
    };

    ComponentDidCatch = (error, info)=> {
        this.setState({hasError: true, errorMessage: error});
    };

    render() {
        if (this.state.hasError) {
            return <h1>this.state.errorMessage</h1>
        } else {
            return this.props.children
        }
    }
}

export default ErrorBoundary;